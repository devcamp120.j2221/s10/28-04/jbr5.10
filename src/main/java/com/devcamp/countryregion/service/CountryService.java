package com.devcamp.countryregion.service;

import java.util.ArrayList;

import com.devcamp.countryregion.model.Country;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CountryService {
    @Autowired
    private RegionService regionservice;

    Country vietnam = new Country("VN", "Viet Nam");
    Country us = new Country("US", "My");
    Country russia = new Country("RUS", "Nga");
    
    public ArrayList<Country> getAllCountries() {
        ArrayList<Country> listCountry = new ArrayList<>();

        vietnam.setRegions(regionservice.getRegionVietNam());
        us.setRegions(regionservice.getRegionUS());
        russia.setRegions(regionservice.getRegionRussia());

        listCountry.add(vietnam);
        listCountry.add(us);
        listCountry.add(russia);

        return listCountry;
    }
}
