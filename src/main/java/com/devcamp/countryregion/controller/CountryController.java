package com.devcamp.countryregion.controller;

import java.util.ArrayList;

import com.devcamp.countryregion.model.Country;
import com.devcamp.countryregion.service.CountryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CountryController {
    @Autowired
    private CountryService countryService;

    @GetMapping("/countries")
    public ArrayList<Country> getAllCountries() {
        ArrayList<Country> allCountry = countryService.getAllCountries();

        return allCountry;
    }

    @GetMapping("/country-info")
    public Country getCountryInfo(@RequestParam(required = true, name="countryCode") String countryCode) {
        ArrayList<Country> allCountry = countryService.getAllCountries();

        Country findCountry = new Country();

        for (Country country : allCountry) {
            if(country.getCountryCode().equals(countryCode)) {
                findCountry = country;
            }
        }

        return findCountry;
    }
}
